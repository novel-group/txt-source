菲亞拉特一邊想辦法去安撫高漲的心情，一邊倒進自己房間的床上。她的臉頰上微微感到火熱，呼吸也非常沉重。

她過往冷靜沉著的思緒，現在卻成了大漩渦一樣在頭腦內肆意亂跑。雖然這並不是良好的狀態，但看來暫時她無法恢復平靜。

使思考變得混亂的是剛剛結束了的談話的內容。

作為路易斯的養母的南因的語言，伴隨著微妙的重量和熱度佔據著費阿拉特的

-對不起。我好像教育得不好。應該再稍微讓他指路易斯學會處世啊。

路易斯剛走出房間，南因以這句話做為開頭。

但是，說實話，芙拉朵並不太記得清當時發生的具體事情。雖然她理解這句話本身是很溫柔的，但內容也完全沒往她心裡去。

因為那時候平靜下來，好女子聽別人話的心理富餘一點也沒有。路易斯所說的話，至今她還未完全接受。

路易斯說，那個魔女一─大聖教聖女阿莉諾的事他至今都沒有忘記。

在他說出這件事的那一瞬間，菲亞拉特理解到自己的黑眼圈被無法忍受的熱所覆蓋。

身體從一側開始變冷，臟腑深涌出的悲哀和憤激分不清的感情，連嗚咽快要露出來了。她第一次知道了，自己的想法可以讓自己的喉嚨燃燒殆盡。

因為是那樣的狀態，老實說芙拉朵慢慢地咀嚼南區芝的言詞的富餘是完全沒有的。只是站著就已經竭盡全力了。如果從旁邊看的話，那個身姿一定非常弱小。

但是南因些，像推測了那樣的拉特，以及卡利婭，菲洛絲的心情一樣地說道。

他的成長是可喜的，但還是有必須要切斷的東西。所以下只不過是提案而已。

有了自己的孩子，他多少會冷靜些吧。男人雖然是個夢想家，但看到嬰兒後就會腳踏實地的。

孩子、子孫、子息、兒女。雖然說法多種多樣，但總之就是被這樣稱呼的存在。
他們與路易斯之間。

一瞬間，反應遲鈍了。芙拉朵還記得，當時仿彿是從頭腦內側像是被錘子打了一樣的觸感。

說南因在說什麼呀。就是這樣的感覺。

充滿了困惑和期待，無法言說的東西，一瞬之間從菲亞拉特的心中開始溢出，喉吃不由得塞住了。

像連這也看穿了一樣，南因之繼續著話語。

並不是說要利用孩子什麼的，只是給他一個契機而已。無論是誰，要與過去割裂的話改變者是必要的。那麼早點改變比較好，生下他的孩子吧。

菲亞拉特看到，南因空的嘴有些猶豫，儘管如此最終也和善地微笑並發言。

以前那傢伙感冒的時候，我曾經讓他喝過蜂蜜酒來代替藥。從那以後他就變得相當喜歡喝這個了。晚上一起喝酒的話，應該睡得很好吧。

雖然是沒有關聯的話，但南因些想說些什麼，在場的所有人都理解了。雖然覺得還有幾句對話，但現在卻不太記得了。
菲亞拉特躺在床上，將細小的手指放在自己的額頭上調整呼吸，呼吸還很熱。

當然，即使是菲亞拉特也並非不期望這樣。如果和他之間能如願望一樣生活，那麼就足夠稱呼為家族了。那是多麼的幸福啊。不，多少幸福可能都無法形容吧。

芙拉朵存在著那樣的甜美的夢想。但與此同時，心中也有著無可奈何的恐怖。

想要和他在一起的時候，自己果真能被他接受嗎？

或許他會牽著別人的手，自己會被狠狠地拒絶吧。變成那樣的時候自己要怎樣的表情才子呢？至少，完全沒有認真起來的心情。恐怕總會有辦法的。

不安和期待，那樣地糾結。這些情動成為波紋爬行在費阿拉特的心胸。果然，思考完全統一不起來，只感覺到像是有沉重的石頭存在於體內一樣。

但是，不能因此就不向前踏出腳步。芙拉朵躺在床上，眯起了黑眼睛。

不管怎麼說，從南因那裡受到熏陶的，不是自己一個人，還有兩個人。

其中一人，統治者菲洛絲・托雷特如何行動至今還不清楚。但是，恐怕現在也不阻止了吧。

雖說也期待著與路易斯牽手的一幕，但菲洛絲這個人，芙拉朵不認為她是喜歡性急地推進事物的性格。菲拉特理解的她是一個注重秩序和正義的人，不會忽
視事物發展的順序。「可惜後來聖女偷跑被菲洛絲髮現了，可能也急起來了

因此，雖然多少有些困惑，但她不會馬上做些什麼吧。

但是，還有一個人。卡麗婭是不同的。在空中飄蕩的銀髮，似乎映入了芙拉朵的視線暗處。

我和她也已經不是短暫的交情了。所以我很明白。她這個人只要想到什麼，就會馬上付諸行動。

甚至認為她的腳上是長著翅膀的寓意行動派），這讓菲亞拉特無可奈何的美慕。

芙拉朵理解到，自己無論從哪方面來說都是冷靜沉思的代表，所以經常也沒有行動。如果能夠像卡麗婭那樣行動，那樣高尚地生活下去，人生將會充滿多少光輝呢？就連這樣的想法，菲亞拉特都只是將其藏在腦海中。

她（卡麗婭）這次不管採取什麼形式，都會付諸行動吧。這一定沒錯。

那麼，自己那個時候怎麼辦？抱著頭躲在床上，等待著路易斯再來迎接我嗎？

然後用這雙眼睛看著誰和他的孩子呢？黑色的眼睛被拉開，芙拉朵的牙齒深深地咬住了嘴唇。身體熱得連疼痛者感覺不到。

只有那個我不能接受，不可能，卡麗婭也好，艾爾蒂絲也好，瑪蒂婭和自稱是青梅竹馬的女人也好，都不可能把和他一起共度的未來交給這些人。

她們不是還有其他的頭銜和光輝對口馬？當然還有其他的生存之道。

但是，自己卻不同。芙拉朵緊緊抓住床單。纖細的手指，略微地咬進床（意思她抓得很緊。）

我所期望的，作為我生存下去的路只有一條，那就是待在他的身邊。

黑色的眼睛一邊搖蕩一邊銳利起來，即使透過充滿室內的黑暗，仍能看見它越來越深，越來越濃。

|──我不是你就不行，你不是我也可以。你不認為這是嚴重的不公平，路易斯？

這就是太過嚴重的差距。正因為如此，那個必須要糾正過來「意思是要讓男主也變得非她不可，恐怖如斯。

芙拉朵的臉上浮現出月艷的笑容，並大大的嘆氣，情緒中帶有什麼情動以外的東西。