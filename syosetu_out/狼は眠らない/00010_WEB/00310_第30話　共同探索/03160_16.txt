１６

「抱歉。看起來有兩隻〈黑肌〉撲向了雷肯。不自覺就」

那兩隻〈黑肌〉沒有頭，雷肯也不會毫無抵抗被撲上來。

就算擔心雷肯，跑到會擋在喬安娜的射線上的位置，果然還是太粗心了。忽略了〈黑肌〉翻滾到喬安娜前方，作為護衛也是失格。然後最重要的是，在這戰鬥完全沒有做到指揮官該有的職責。

（呼嗯）
（這是該由我下指示的場面）

「在這階層在戰鬥一次吧。但是這次要變更職務。突入順序是，阿利歐斯，秦格，布魯斯卡，我，喬安娜」
「誒？雷肯要保護我嗎？」
「對。用這個」

雷肯稍微舉高了裝在左手的護甲形狀的〈沃爾肯之盾〉。

「〈沃爾肯之盾〉嗎。就覺得是不是那個，果然是阿」
「第一次見到呢」
「什麼什麼，那個〈沃爾肯之盾〉」
「喬安娜小姐。〈沃爾肯之盾〉是物理攻擊和魔法攻擊都能防禦的，非常優秀的盾牌」
「嘿，這樣阿」
「秦格跟布魯斯卡，先去把〈赤肌〉打倒。他們攻擊同伴也不會在意，所以留在後頭會不好處理。最好當做不會再像剛才那樣集合起來」
「但是那樣的話，就應付不了〈黑肌〉了喔」
「讓三隻過來這邊也無妨。阿利歐斯、秦格和布魯斯卡先各自打倒一隻〈赤肌〉，之後就跟附近的敵人戰鬥。最後的〈赤肌〉，就讓附近的人打倒」
「我呢？」
「首先就跟之前一樣，近房間後就打倒一隻〈黑肌〉」
「好喔」
「如果附近有〈黑肌〉，我也會打倒一隻。之後就用〈雷矢〉把〈黑肌〉一隻一隻引誘過來。狙擊軀幹就好，不用狙擊腳」

狙擊軀幹當然比狙擊腳掌簡單。也就是減低了喬安娜的負擔。

「不錯呢。讓三隻過去也無妨？一隻一隻引誘出來？真有自信呢」
「雷肯的攻擊力很驚人呢。竟然連同頭部周圍的防禦把〈黑肌〉的投給砍下來。那把劍，是有恩寵嗎？」
「對。跟那斧頭一樣。那麼，到附近的房間戰鬥看看吧」
「先等一下。喬安娜還沒準備好。魔法使不在戰鬥跟戰鬥之間間隔充分時間，就不能好好使出力量」
「沒問題的喔，秦格」
「但是」

不知為何，阿利歐斯一邊微笑，一邊交互看著喬安娜和掛念喬安娜的秦格的臉。

「明明才剛擊出〈豪炎斬〉，但是魔力卻不斷地湧出。這種是還是第一次。雷肯。那個，真的是魔力回復藥呢」
「就是那麼說的」
「這個，難道是琵玖的藥嗎？這個城鎮拜琵玖所賜，有好幾位優秀的藥師，雖然不會為傷藥和萬能藥感到困擾，但是入手不了魔力回復藥。你有這城鎮的藥師的人脈嗎？」
「沒有這城鎮的藥師的人脈」
「嘿？」

雷肯把〈因邱雅多羅之首飾〉收了起來。因為會妨礙到在正後方擊出魔法的喬安娜。

之後，五人進入附近的房間戰鬥。
雷肯準確地預言了魔獸的位置關係，下了戰鬥的指示。
阿利歐斯說中了有恩寵的魔獸，讓〈遊手好閒〉的三人很驚訝。
喬安娜最初雖然戰戰兢兢的，但雷肯那鐵壁般的防禦似乎很讓人安心，竟然擊出了〈豪炎斬〉兩次。
布魯斯卡愉快地揮舞兩把斧頭。

雷肯對恩寵品〈鑑定〉時，〈遊手好閒〉的三人都愕然了。

「不錯呢，不錯！果然五人隊伍最棒了。要是有十個人，同伴就會妨礙到，有夠難擊出魔法的」
「嗯。我也很好戰鬥。要是同伴有十人，就不能自由揮舞雙手的斧頭，所以都得負責護衛工作，好久沒這樣爽快戰鬥了阿」
「老夫也很好戰鬥。果然戰鬥空間有餘裕是好事阿」
「而且雷肯給的魔力回復藥，效果好厲害！現在也在源源不絶地滾出魔力。雷肯，這個，效果會持續多久？」
「現在是最有效果的時間，效果會漸漸減少。但效果還是會持續半天」
「半天！這還真是。這真厲害阿。原來如此。難怪〈那一側〉的傢伙們會出大錢買斷。很貴吧？」

這時候還不知道〈那一側〉是指什麼，之後得知了是指在深於百階層的階層戰鬥的人。

「是我自己做的藥」
「又來了又來了。那個夠了啦。做得了這種東西的話，就算不做什麼冒険者也能賺大錢喔」
「琵玖這藥師的魔力回復藥很貴嗎？」
「很貴喔。領主會買斷來賣給他領的領主，確保給〈那一側〉的傢伙們用的，所以有被賣出來的非常少。比起這個，不接著打嗎。我現在可是最棒的狀態了」

因為喬安娜有幹勁，所以能輕鬆誘導〈遊手好閒〉。

五人挑戰了九十階層的大型個體，輕易地擊破了。然後，和九十二階層的大型個體的戰鬥，以及和九十三階層的大型個體的戰鬥都獲勝了。
花時間的是階梯的移動。從九十二階層到九十三階層的階梯也是用走的下去，打倒九十三階層的大型個體後，走到了九十四階層一次。

不需要〈鼠〉帶路就能確實地探查到大型個體的房間的雷肯的魔法，讓〈遊手好閒〉的三人頻頻感到羨慕。到了九十階層帶，能帶路的〈鼠〉很少，不預約就僱用不了。雖然也有賣地圖，但要在寬廣的階層裡正確地抵達那場所，就算邊看著地圖也不容易的樣子。迷宮的地圖這種東西，畫家各有自己的習慣，會在抄寫中變成很難理解的地圖，所以這也是沒辦法的。

最後，這一天的聯合隊伍，在七十九階層一次，八十九階層兩次，九十階層一次，九十二階層一次，九十三階層一次，共計戰鬥了六次。而且後半三次的對手都是大型個體。戰鬥的密度很高。
出了五把恩寵品武器。

離開迷宮時已經晚了，但還是直接去了收購所。
賣到了白金幣一枚和大金幣兩枚這金額。
〈遊手好閒〉的三人似乎欣喜若狂的樣子。

雷肯對收購所的鑑定感到佩服。
把想鑑定的物品放到櫃台上後，坐在另一側的鑑定人就會迅速鑑定，馬上製作鑑定書。那鑑定書會送到坐在後方的事務官那邊，在鑑定書上寫上評定額。這會交還給鑑定人，鑑定人會在紙張上寫上評定額，有名稱就會寫上名稱，有恩寵就會寫上恩寵名，然後交給委託人。

委託人如果說要賣那物品的話，鑑定書就會送到後方，並準備金額。
委託人申請的話，能拿到鑑定書的副本。但在這場合需要鑑定費。鑑定費的話，淺層品要銀幣一枚，中層品要大銀幣一枚，深層品要金幣一枚。因為是預先聽了評定額再申請鑑定，所以這費用非常有良心。不過，如果拜託鑑定茨波魯特迷宮出的物品以外的，就要先付評定額的十分之一。從別處帶來的物品，不能只聽收購額就了事。

不論哪個櫃台的鑑定人都流利地接連進行鑑定。後方有替補的鑑定人們在待命。
鑑定人絶對不會碰鑑定品。想移動時會叫持有者來動。這也讓雷肯感到佩服。

之後才知道，大量的鑑定人並非全部由領主直接僱用，大部分都是商人們派遣的。根據派遣人數，商人們能分攤到迷宮品。

雷肯對共同探索感到滿足。
戰鬥急遽變輕鬆了。

果然兩人對付十隻很嚴峻。
喬安娜盡速屠殺掉一隻〈黑肌〉就會剩九隻，阿利歐斯也會盡速打倒一隻〈赤肌〉，所以會是五人對付八隻。

〈黑肌〉和〈赤肌〉相互混雜並擊來魔法雖然麻煩，但如果是阿利歐斯、秦格和布魯斯卡這三人，就能十拿九穩地一邊牽制四隻〈赤肌〉並打倒〈黑肌〉。被喬安娜引誘出來的魔獸會被雷肯以〈奧多之劍〉一擊打倒。

〈奧多之劍〉雖然是威力高的劍，但不蓄好力，揮出正確的刃筋的話，就發揮不了那威力。五人隊伍的話就能保持餘裕戰鬥。

這一晚的酒很好喝。