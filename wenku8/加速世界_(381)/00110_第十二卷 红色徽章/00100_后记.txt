大家好，我是川原礫，在此為各位讀者送上《加速世界12紅色徽章》

我想如果各位讀者已經看完本集，應該就會知道副標題的含意。到了這一集，從第一集就一直拖到現在的「黑之王扣光初代紅之王點數事件」終於揭曉詳情了。

我有個壞習慣，就是會寫到一半才接連追加很多設定，但這件事卻難得維持住了從一開始就想好的構想。白之王White　Cosmos終於登場……是沒有啦，但至少名字出場了（笑），黑雪公主與春雪要如何對抗她呢？我想這就要請各位讀者期待續集了！也就是說，這次又弄成待績了，非常對不起！

除此之外，本集中還有一個在上次後記中提到我從「加速世界虛擬角色設計賽」採用的對戰虛擬角色「Chocolat　Puppeteer」登場。她的戲分比我當初想像中要重要得多，對此我也覺得十分驚喜，但更加意想不到的，卻是責任編輯三木對這位Chocolat小姐執著得不得了（笑）。聽到他下令：「多寫一點舔她的場面」，我回答：「可是這是虛擬角色耶！」結果蒙他開導說：「所以才贊啊！」我當場恍然大悟，所以就讓她被舔了個夠。為本作設計這位虛擬角色的几彌なでみ小姐，非常對不起！同時也非常謝謝您！

至於虛擬角色設計賽，在二次徵稿時也將再發表三位參賽者的應徵作品，採用到原作之中。這些虛擬角色也會依序登場（只是篇幅多半會受限……），敬請各位讀者期待！

等到本書出版，相信電視版動畫的播放也已經漸入佳境，而在動畫版第6與第7話（在原作則是第10集）登場的「四大元素」之一的Aqua　Current，終於在本集當中再度登場了。我打算從下一集開始，讓她成為固定主力活躍，但這一來又會讓春雪身邊的女性變多，真是令人戰戰兢兢呀！

除了她以外，這次也繼續增加了許多新角色。感謝HIMA老師把他們都畫得可愛又帥氣；責任編輯三木先生，讓您費神讀這種跟事前開會時講的完全不一樣的原稿，真的是非常謝謝您。

第13集預計間隔會久一點，但是下次劇情也會告一段落，還請各位讀者繼續給予本作品支持與愛護！

二〇一二年六月某日　　川原礫
