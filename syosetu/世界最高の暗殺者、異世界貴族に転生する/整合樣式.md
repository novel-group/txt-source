---
LocalesID: 世界最高の暗殺者、異世界貴族に転生する
LocalesURL: https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E4%B8%96%E7%95%8C%E6%9C%80%E9%AB%98%E3%81%AE%E6%9A%97%E6%AE%BA%E8%80%85%E3%80%81%E7%95%B0%E4%B8%96%E7%95%8C%E8%B2%B4%E6%97%8F%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%99%E3%82%8B.ts
---
__TOC__

[世界最高の暗殺者、異世界貴族に転生する](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E4%B8%96%E7%95%8C%E6%9C%80%E9%AB%98%E3%81%AE%E6%9A%97%E6%AE%BA%E8%80%85%E3%80%81%E7%95%B0%E4%B8%96%E7%95%8C%E8%B2%B4%E6%97%8F%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%99%E3%82%8B.ts)  
總數：32／33

# Pattern

## 羅格

### patterns

- `ルーグ`

## 托瓦哈迪

### patterns

- `トウアハーデ`

## 基安

### patterns

- `(?<![ァ-ヴーｱ-ﾝﾞｰ])(キアン?)(?![ァ-ヴーｱ-ﾝﾞｰ])`

## 艾莉絲

### patterns

- `エスリ`

## 克勞迪亞

### patterns

- `クローディア`
- `克勞迪亞`
- `克勞迪雅`

## 迪雅

### patterns

- `迪亞`
- `ディア`

## 維科內

### patterns

- `ヴィコーネ`

## 塔爾特

### patterns

- `タルト`
- `塔特爾特`

## 埃波納

### patterns

- `エポナ`

## 利安諾

### patterns

- `里安農`
- `リアンノン`

## 瑪哈

### patterns

- `マーハ`

## 法莉娜

### patterns

- `法利娜`
- `法琳娜`
- `法利納`
- `ファリナ`
- `法莉娜`

## 娜芳

### patterns

- `ネヴァン`

## 米娜

### patterns

- `ミーナ`

## 諾伊斯

### patterns

- `ノイシュ`

## 萊克拉

### patterns

- `リクラ`

## 羅馬爾格

### patterns

- `ローマルング`
- `羅馬格`

## 雷切爾

### patterns

- `レイチェル`

## 托里

### patterns

- `(?<![ァ-ヴーｱ-ﾝﾞｰ])([托]里)(?![ァ-ヴーｱ-ﾝﾞｰ])`

## 維納斯

### patterns

- `ウェヌス`

## 歐露娜

### patterns

- `オルナ`
- `奧露娜`
- `奧爾娜`
- `奧爾納`
- `歐魯娜`
- `歐露娜`

## 伊魯古

### patterns

- `イルグ`
- `伊爾格`

## 巴洛爾

### patterns

- `バロール`

## 萊奧格爾

### patterns

- `ライオゲ`

## 阿爾凡

### patterns

- `艾爾凡`
- `阿爾凡`
- `アルヴァン`
- `阿爾文`
- `巴爾凡`

## 約翰布魯

### patterns

- `ジョンブル`
- `約翰布魯`
- `約翰布爾`

## 斯翁格爾

### patterns

- `スオンゲル`

## 迪安凱特

### patterns

- `ディアンケト`

## 成長界限突破

### patterns

- `成長界限突破`
- `突破增長界限`
- `突破成長界限`

## 盧南鱒魚

### patterns

- `ルナンマス`
- `魯南馬斯`
- `盧南鱒魚`
- `盧南馬斯`

## 鱒魚

### patterns

- `マス`
- `鱒魚`

## 阿卡西

### patterns

- `奧克西`
- `アカシック`


